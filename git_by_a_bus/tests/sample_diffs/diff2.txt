diff --git a/README.txt b/README.txt
index c798d4c..b947ee2 100644
--- a/README.txt
+++ b/README.txt
@@ -10,8 +10,6 @@ http://dev.hubspot.com/bid/57694/Git-by-a-Bus
 
 ## Bugs
 
-Known not to work currently across symlinks.
-
 If you find any, let me know on github or by email at {first
 initial}{last name}@{my company}.com, all lowercase.
 
