# Team Proposal

## List your other team members below

  * Regina Locicero rtl3971@rit.edu
  * Aidan Kahrs axk4545@rit.edu
  * Quintin Reed qar4778@rit.edu

## Which project did your team choose?

[Jekyll](https://jekyllrb.com/)

## 2-liner description of the project?

Jekyll is a static site generator built with Ruby. It takes markdown content and generates a static html and css site.

## What will each team member's role be?

Quintin Reed: Research

Aidan Kahrs: Code and Git by a bus

Regina Locicero: Documentation

All of us will be a part of getting git by a bus running and will be a part of writing up the report and doing analysis.


# Source Code Repository URL?

[Jekyll GitHub source](https://github.com/jekyll/jekyll)

[Our repo with our documentation](https://github.com/axk4545/hfoss-commarch)


## List your upstream Mentors below:

  * [parkr](https://github.com/parkr) email@byparker.com
  * [jaybe-jekyll](https://github.com/jaybe-jekyll) 
        

## How will you communicate with them? (i.e. IRC Channel, Email Addresss, mail lists, issue trackers, etc...)

Probably through their IRC at #jekyll on irc.freenode.net.

## What are the easy parts?

  * Prepping for the presentation
  * Writing the report

## What are the hard parts?

  * Getting git by a bus to run
  * Communicating with upstream/mentor

## How will you overcome both?

For our hard parts, specifically getting git by a bus to run properly, we will be all trying to run it separately on our own. 
We will also be communicating with each other and with those who have done the project before specifically with the instructor as well with our issues.
Our hard deadline for the git by a bus issue will be the 25th. Though we hope to get it done by the end of the week.


